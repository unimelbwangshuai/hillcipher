package SubstitutionCipher;

import Numbers.ComputingInverse;

public class HillCipher {
	static char[] plainLetterTable = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

	private static final int LENGTH = 3;
	/**
	 * For example: 
	 * 1 2 3
	 * 2 2 1
	 * 3 4 3
	 * @param A
	 * @return
	 */
	public int[][] inverse(int[][] A){
		int[][] inverseA = new int[LENGTH][LENGTH];
		int det = determinant(A);
		int[][] adjMatrix = adjugateMatrix(A);
		int inverseDet = ComputingInverse.getInverse(det, 26);
		for(int i=0; i<adjMatrix.length; i++){
			for(int j=0; j<adjMatrix.length; j++){
				inverseA[i][j] = Math.floorMod(adjMatrix[i][j]*inverseDet, 26);
			}
		}
		
		/*
		System.out.println("inverseA start........");
		for(int i=0; i<inverseA.length; i++){
			for(int j=0; j<inverseA.length; j++){
				System.out.print(inverseA[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("inverseA end........");
		*/
		return inverseA;
	}
	
	/**
	 * Get the determinant of a matrix 
	 * @param A
	 * @return
	 */
	public int determinant(int[][] A){
		int determinant;
		determinant = A[0][0]*A[1][1]*A[2][2] + A[1][0]*A[2][1]*A[0][2] + A[2][0]*A[0][1]*A[1][2]
				- A[2][0]*A[1][1]*A[0][2] - A[1][0]*A[0][1]*A[2][2] - A[0][0]*A[2][1]*A[1][2];
		determinant = Math.floorMod(determinant, 26);
		//System.out.println("determinant="+determinant);
		return determinant;
	}
	
	/**
	 * Get adjugate matrix
	 * @param A
	 * @return
	 */
	public int[][] adjugateMatrix(int[][] A){
		int[][] tmpA = new int[A.length][A.length];
		
		for(int i=0; i<A.length; i++){
			for(int j=0; j<A.length; j++){
				tmpA[i][j]=subdeterminant(i,j,A);
			}
		}
		
		for(int i=0; i<tmpA.length; i++){
			for(int j=i; j<tmpA.length; j++){
				int tmp = tmpA[i][j];
				tmpA[i][j] = tmpA[j][i];
				tmpA[j][i] = tmp;
			}
		}
		
		return tmpA;
	}
	
	/**
	 * get subdeterminant
	 * @param i
	 * @param j
	 * @param A
	 * @return
	 */
	public int subdeterminant(int i, int j, int[][] A){
		int subdeterminant;
		int[] submatrix = new int[LENGTH+1];
		int count=0;
		for(int m=0; m<A.length; m++){
			for(int n=0; n<A.length; n++){
				if(m==i||n==j){
					continue;
				}
				submatrix[count++]=A[m][n];
			}
		}
		subdeterminant = (submatrix[0]*submatrix[3] - submatrix[1]*submatrix[2]) * (int)Math.pow(-1, i+j);
		//subdeterminant = (submatrix[0]*submatrix[3] - submatrix[1]*submatrix[2]);
		return subdeterminant;
	}
	
	/**
	 * Matrix multiplication
	 * @param A
	 * @param B
	 * @return
	 */
	public int[] multiplication(int[] A, int[][] B){
		int[] product = new int[A.length];
		
		for(int j=0; j<A.length; j++){
			product[j] = Math.floorMod((A[0]*B[0][j] + A[1]*B[1][j] +A[2]*B[2][j]),26);
		}
		return product;
	}
	
	/**
	 * abc -> [0, 1, 2]
	 * @param word
	 * @return
	 */
	public int[] string2matrix(String word){
		int[] matrix = new int[LENGTH];
		for(int i=0; i<word.length(); i++){
			matrix[i] = (int)word.charAt(i)-97;
		}
		return matrix;
	}
	
	/**
	 * [1,2,3] -> abc
	 * @param matrix
	 * @return
	 */
	public String matrix2string(int[] matrix){
		String word = "";
		for(int i=0; i<matrix.length; i++){
			word += (char)(matrix[i]+97);
		}
		return word;
	}
	
	
	/**
	 * Encryption
	 * @param plaintext
	 * @param key
	 * @return
	 */
	public String encrypt(String plaintext, int[][] key){
		String ciphertext = "";		
		for(int i=0; i<plaintext.length(); i+=3){
			String word = ""+ plaintext.charAt(i+0) + plaintext.charAt(i+1) + plaintext.charAt(i+2);
			int[] matrix = string2matrix(word);
			matrix = multiplication(matrix, key);
			String cipher = matrix2string(matrix);
			ciphertext += cipher;
		}
		return ciphertext.toUpperCase();
	}
	
	/**
	 * Decryption
	 * @param ciphertext
	 * @param key
	 * @return
	 */
	public String decrypt(String ciphertext, int[][] key){
		String plaintext="";
		ciphertext = ciphertext.toLowerCase();
		for(int i=0; i<ciphertext.length(); i+=3){
			String word = ""+ ciphertext.charAt(i+0) + ciphertext.charAt(i+1) + ciphertext.charAt(i+2);
			int[] matrix = string2matrix(word);
			int[][] inverseKey = inverse(key);
			matrix = multiplication(matrix, inverseKey);
			String plain = matrix2string(matrix);
			plaintext += plain;
		}
		return plaintext;
	}
	
	/**
	 * We we attack hill cipher with m plaintext-ciphertext pairs.
	 * @param plaintext
	 * @param ciphertext
	 * @return
	 */
	public int[][] stealKey(String plaintext, String ciphertext){
		plaintext = plaintext.toLowerCase();
		ciphertext = ciphertext.toLowerCase();
		
		int[][] key = new int[LENGTH][LENGTH];
		int[][] plainMatrix = new int[LENGTH][LENGTH];
		int[][] cipherMatrix = new int[LENGTH][LENGTH];
		
		int row=0;
		for(int i=0; i<plaintext.length(); i+=3){
			String plain = ""+ plaintext.charAt(i+0) + plaintext.charAt(i+1) + plaintext.charAt(i+2);
			
			int[] subPlainMatrix = string2matrix(plain);
			for(int k=0; k<subPlainMatrix.length; k++){
				plainMatrix[row][k] = subPlainMatrix[k];
			}
			
			String cipher = ""+ ciphertext.charAt(i+0) + ciphertext.charAt(i+1) + ciphertext.charAt(i+2);
			int[] subCipherMatrix = string2matrix(cipher);
			for(int k=0; k<subCipherMatrix.length; k++){
				cipherMatrix[row][k] = subCipherMatrix[k];
			}
			row++;
		}
		
		/*
		System.out.println("cipherMatrix");
		for(int i=0; i<cipherMatrix.length; i++){
			for(int j=0; j<cipherMatrix.length; j++){
				System.out.print(cipherMatrix[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("==========");
		*/
		plainMatrix = inverse(plainMatrix);
		
		for(int i=0; i<LENGTH; i++){
			key[i] = multiplication(plainMatrix[i], cipherMatrix);
		}
		
		return key;
	}
	/**
	 * Test
	 * @param args
	 */
	public static void main(String[] args) {
		HillCipher hc = new HillCipher();
		
		int[][] key = { {8,23,24},
						{16,16,7},
						{19,9,23}};
		String plaintext = "philosophersaskcanhumaningenuityconcoctacipherwhichhumaningenuitycannotresolve";
		String ciphertext = hc.encrypt(plaintext, key);
		System.out.println(ciphertext);
		
		
		String ciphertext2 = "UJVEPWRBEWGFKOSDHJGRMWUPQPEEPMCHUUCFLFHCAQWZHXAVVTDGRMWUPQPEEPMCHUNNALZCOMYGBJ";
		String plaintext2 = hc.decrypt(ciphertext2, key);
		System.out.println(plaintext2);
		/*
		String plaintext = "PHILOSOPH";
		
		String ciphertext = "UJVEPWRBE";
		int[][] key = hc.stealKey(plaintext, ciphertext);
		key = hc.inverse(key);
		System.out.println("memeda");
		for(int i=0; i<key.length; i++){
			for(int j=0; j<key.length; j++){
				System.out.print(key[i][j]+" ");
			}
			System.out.println();
		}
		*/
	}
}
